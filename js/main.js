function send_login_request(params, success_function, error_function){
	// console.log("Send request call");
	console.log(params)
	$.ajax({
	  method: "POST",
	  crossDomain: false,
	  processData: false,
	  headers: {
      "content-type": "application/json",
    },
	  url: "https://us-central1-kaya-99166.cloudfunctions.net/userManagement",
	  // url: "http://localhost:3000/check_user_management",
	  data: params,
	  success: success_function,
	  error: error_function
	});
}

$(document).ready(function() {
	// Header Scroll
	$(window).on('scroll', function() {
		var scroll = $(window).scrollTop();

		if (scroll >= 50) {
			$('#header').addClass('fixed');
		} else {
			$('#header').removeClass('fixed');
		}
	});

	// Waypoints
	$('.work').waypoint(function() {
		$('.work').addClass('animated fadeIn');
	}, {
		offset: '75%'
	});
	$('.download').waypoint(function() {
		$('.download .btn').addClass('animated tada');
	}, {
		offset: '75%'
	});

	// Fancybox
	$('.work-box').fancybox();

	// Flexslider
	$('.flexslider').flexslider({
		animation: "fade",
		directionNav: false,
	});

	// Page Scroll
	var sections = $('section')
		nav = $('nav[role="navigation"]');

	$(window).on('scroll', function () {
	  	var cur_pos = $(this).scrollTop();
	  	sections.each(function() {
	    	var top = $(this).offset().top - 76
	        	bottom = top + $(this).outerHeight();
	    	if (cur_pos >= top && cur_pos <= bottom) {
	      		nav.find('a').removeClass('active');
	      		nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
	    	}
	  	});
	});
	nav.find('a').on('click', function () {
	  	var $el = $(this)
	    	id = $el.attr('href');
		$('html, body').animate({
			scrollTop: $(id).offset().top - 75
		}, 500);
	  return false;
	});

	// Mobile Navigation
	$('.nav-toggle').on('click', function() {
		$(this).toggleClass('close-nav');
		nav.toggleClass('open');
		return false;
	});	
	nav.find('a').on('click', function() {
		$('.nav-toggle').toggleClass('close-nav');
		nav.toggleClass('open');
	});

	$("#login_form").on("submit", function(e){
		e.preventDefault();
		var email = $(this).find(".email_field").val();
		var password = $(this).find(".password_field").val();

  	var return_url = $(this).attr("action");
		var params = {};
  	params["action_name"] = "login";
  	params["email"] = email;
  	params["password"] = password;
  	params = JSON.stringify(params);

  	send_login_request(params, function(data){
  		console.log("Success");
  		console.log(data);
  		data = JSON.parse(data);
  		sessionStorage.setItem('session_id', data.user.session_id);
  		sessionStorage.setItem('user_id', data.user.id);
  		sessionStorage.setItem('user_name', data.user.name);
  		window.location = return_url;
  	}, function(err){
  		console.log("Error");
  		console.log(err.message);
  	});

	});
});