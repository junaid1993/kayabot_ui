function send_request(params, success_function, error_function){
	// console.log("Send request call");
	// console.log("Parameters: " + params);
	// console.log("==-=-=-=-=-=-=-=-=-=-");

	$.ajax({
	  type: "POST",
	  headers: {
      'Content-Type':'application/json'
    },
	  url: "https://asia-northeast1-kaya-99166.cloudfunctions.net/domainMatching",
	  // url: "http://localhost:3000",
	  data: params,
	  success: success_function,
	  error: error_function
	});
}

var botui = new BotUI('lost-and-found-bot'),
  address = 'Singapore';

botui.message
  .bot({
  	cssClass: "bot-message-container",
  	content:"Hi let's get started"
  })
  .then(function () {
    return botui.action.button({
      delay: 400,
      addMessage: false, // so we could the address in message instead if 'Existing Address'
      action: [{
        text: 'Get Started',
        value: 'Get Started'
      }, {
      	text: 'End Chat',
        value: 'end'
      }]
    })
}).then(function (res) {
  botui.message.human({
    content: res.text,
    cssClass: 'human-message-container'
  });
	if(res.value == "end") {
    end();
  }
  else{
  	var params = {};
  	params["lang"] = "en";
  	params["query"] = res.value;
  	params["sessionId"] = "id:"+sessionStorage.getItem('user_id')+","+"name:"+sessionStorage.getItem('user_name');
  	var originalRequest = {};
  	originalRequest["source"] = "webUI";
  	originalRequest["data"] = {};
  	originalRequest["data"]["userId"] = sessionStorage.getItem('user_id');
  	originalRequest["data"]["userName"] = sessionStorage.getItem('user_name');
  	params["originalRequest"] = originalRequest;
  	// params["user_id"] = sessionStorage.getItem('user_id')
  	// params["user_name"] = sessionStorage.getItem('user_name')
  	params = JSON.stringify(params);
  	send_request(params, function(data){
  		// console.log(data);
  		add_response(data);
  	}, function(err){
  		console.log(err.message);
  	});
  }
});

var end = function () {
  botui.message
  .bot({
    delay: 400,
    loading: true,
    content: 'Good Bye!',
    cssClass: 'bot-message-container'
  })
  botui.message
  .bot({
  	delay: 400,
  	loading: true,
  	content: "I am available here for you if you want me for anything",
  	cssClass: 'bot-message-container'
  })
  .then(function () {
    return botui.action.text({
      // delay: 1000,
      addMessage: false,
      action: {
        size: 30,
        // icon: 'map-marker',
        value: "", // show the saved address if any
        placeholder: 'Enter Your Response Here'
      }
    })
  })
  .then(function(res){
		botui.message.human({
			content: res.value,
			cssClass: 'human-message-container'
		})
	  var params = {}
  	params["query"] = res.value
  	params["lang"] = "en"
  	params["sessionId"] = "id:"+sessionStorage.getItem('user_id')+","+"name:"+sessionStorage.getItem('user_name')
  	// params["user_id"] = sessionStorage.getItem('user_id')
  	// params["user_name"] = sessionStorage.getItem('user_name')
  	var originalRequest = {};
  	originalRequest["source"] = "webUI";
  	originalRequest["data"] = {};
  	originalRequest["data"]["userId"] = sessionStorage.getItem('user_id');
  	originalRequest["data"]["userName"] = sessionStorage.getItem('user_name');
  	params["originalRequest"] = originalRequest;

  	params = JSON.stringify(params)
  	send_request(params, function(data){
  		// console.log(data);
  		add_response(data)
  	}, function(err){
  		console.log(err.message)
  	});
	});
}

// Extract messages where platform is equal to provided platform
function check_for_platform(messages, platform){
		
	// For V2
	// fb_messages = messages.filter(function(message){
	// 	return message["platform"].toLowerCase() == platform;
	// });

	fb_messages = messages.filter(function(message){
		return message["platform"] == platform;
	});

	if(fb_messages.length > 0){
		return fb_messages;
	}
	return messages;
}

// Sort messages on the basis of attribute given
function sort_messages(messages, attribute){
	messages = messages.sort(function(a, b) {
    return parseFloat(a[attribute]) - parseFloat(b[attribute]);
	});
	return messages;
}


function filter_message(messages){
	// Some messages are coming where content is zero
	messages = messages.filter(function(message){
		if ('speech' in message)
			return (message["speech"] != ""); // speech should not be blank
		else if ('title' in message)
			return (message["title"] != ""); // title should not be blank
		else
			return false; // everthing else should be filtered
	});
	return messages;
}
// DialogFlow message parsing logic
function parse_response(response){
	// console.log("Parsing Response")
	messages = response["result"]["fulfillment"]["messages"] // V1
	
	// messages = response[0].queryResult.fulfillmentMessages // V2
	// This function will check if there is any messages from facebook
	// and if there is at-least one message given platform it will fetch and show only those messages
	messages = check_for_platform(messages, "facebook");
	// Sort messages on the basis of type of messages type can be either 0 or 2
	
	messages = sort_messages(messages, "type"); // it's necessary for v1 not for v2

	// Filter Invalid messages
	messages = filter_message(messages); // it's necessary for v1 not for v2

	// Standarize the structure of object
	// in some cases text of the message appears in speech and in some cases it appears in title
	// this processing is to strandarize this 
	var processed_reponse = {};
	processed_reponse["action_button"] = false;
	var processed_messages = [];
	for(var i = 0; i < messages.length; i++){
		// console.log(messages[i])
		
		// for V1
		temp = {};
		if ('speech' in messages[i])
			temp["text"] = messages[i]["speech"];
		else if ('title' in messages[i])
			temp["text"] = messages[i]["title"];

		if(messages[i].replies){
			processed_reponse["action_button"] = true;
			temp["replies"] = messages[i].replies;
		}

		// For V2

		// temp = {};
		// temp["message"] = messages[i].message;
		// if(messages[i].message == "text"){
		// 	temp["text"] = messages[i].text.text
		// }
		// else if(messages[i].message == "quickReplies"){
		// 	processed_reponse["action_button"] = true;
		// 	temp["text"] = messages[i].quickReplies.title	
		// 	temp["replies"] = messages[i].quickReplies.quickReplies
		// }


		processed_messages[i] = temp;
	}

	processed_reponse["messages"] = processed_messages;
	return processed_reponse;
}

function add_response(response){
	// console.log("Adding Response");
	var processed_reponse = parse_response(response);
	var messages = processed_reponse["messages"]
	for(i = 0; i < messages.length; i++){
		
		// For V1
		var msg = botui.message.bot({
			delay: 400,
			loading: true,
  		content: messages[i]["text"],
  		cssClass: 'bot-message-container'
		})

		// For V2
		// if(messages[i].message == "text"){
		// 	var msg = botui.message.bot({
		// 		delay: 400,
		// 		loading: true,
	 //  		content: messages[i].text[0],
	 //  		cssClass: 'bot-message-container'
		// 	})
		// }
		// else if(messages[i].message == "quickReplies"){
		// 	var msg = botui.message.bot({
		// 		delay: 400,
		// 		loading: true,
	 //  		content: messages[i].text,
	 //  		cssClass: 'bot-message-container'
		// 	})	
		// }

		if(processed_reponse["action_button"] == true && messages[i]["replies"]){
			var actions = [];
			for(var j = 0; j < messages[i]["replies"].length; j++){
				actions[j] = {}
				actions[j]["text"] = messages[i]["replies"][j];
				actions[j]["value"] = messages[i]["replies"][j];		
			}
			msg = msg.then(function () {
		    return botui.action.button({
		      addMessage: false, // so we could the address in message instead if 'Existing Address'
		      action: actions
			  })
			})
			setTimeout(function(){ 
				// $("html").scrollTop($("html").height());
				$(".botui-container").scrollTop($(".botui-messages-container").height());
				// $("html").attr("style", "height: " + $("html").height() + "px");
			}, 500);
		}
		else{
			msg = msg.then(function () {
	      return botui.action.text({
	        // delay: 1000,
	        addMessage: false,
	        action: {
	          size: 30,
	          // icon: 'map-marker',
	          value: "", // show the saved address if any
	          placeholder: 'Enter Your Response Here'
	        }
	      })
	    })
		}
		msg.then(function(res){
			botui.message.human({
				content: res.value,
				cssClass: 'human-message-container'
			})
		  var params = {}
	  	params["query"] = res.value
	  	params["lang"] = "en"
	  	params["sessionId"] = "id:"+sessionStorage.getItem('user_id')+","+"name:"+sessionStorage.getItem('user_name')
	  	// params["user_id"] = sessionStorage.getItem('user_id')
	  	// params["user_name"] = sessionStorage.getItem('user_name')
	  	var originalRequest = {};
	  	originalRequest["source"] = "webUI";
	  	originalRequest["data"] = {};
	  	originalRequest["data"]["userId"] = sessionStorage.getItem('user_id');
	  	originalRequest["data"]["userName"] = sessionStorage.getItem('user_name');
	  	params["originalRequest"] = originalRequest;
	  	
	  	params = JSON.stringify(params)
	  	send_request(params, function(data){
	  		// console.log(data);
	  		add_response(data);
	  	}, function(err){
	  		console.log(err.message)
	  	});
		})
	}
}