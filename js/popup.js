$(document).ready(function(){
	$('.load_chat').on('click',function(){
		$(this).addClass("buttonDown");	
		setTimeout(function(){
			$(".load_chat").addClass("hidden");
			$("#iframe_window").css('display','inline');
		}, 100);
		// $('iframe').contents().find("body").find(".botui-app-container").find('.botui-container').scrollTop($('.botui-container').height());
		
		// $('#iframe_window').contents().find('body').find(".botui-app-container").scrollTop($('.botui-container').height());
	});
	$('.close_chat').on('click',function(){
		console.log("Going to close Window");
		$('#iframe_window', window.parent.document).css("display", "none");
		$('.load_chat', window.parent.document).removeClass("hidden");
		$('.load_chat', window.parent.document).removeClass("buttonDown");
		// $("#chat_button", window.parent.document).removeClass("hidden");
		// parent.closeIframe();
		// $(".load_chat").removeClass("hidden");
	});
	if(sessionStorage.getItem('session_id')){
		$(".load_chat").removeClass("hidden");
		$(".login_button").addClass("hidden");
		$(".logout_button").removeClass("hidden");
	}
	else{
		$(".load_chat").addClass("hidden");
		$(".login_button").removeClass("hidden");
		$(".logout_button").addClass("hidden");
	}
	$(".logout_button").on("click", function(e){
		sessionStorage.removeItem('session_id');
		sessionStorage.removeItem('user_id');
		sessionStorage.removeItem('user_name');
	});
})
